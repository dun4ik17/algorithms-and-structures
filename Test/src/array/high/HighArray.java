package array.high;

import array.interfaces.AbstractArray;
import array.interfaces.ArrayMaxOperation;

public class HighArray extends AbstractArray implements ArrayMaxOperation {

    public HighArray(int maxSize) {
        super(maxSize);
    }

    @Override
    public int getMaxId(){
        int maxId = 0;

        if(size == 0) {
            return -1;
        }

        for (int i = 1; i < size; i++) {
            if(array[i] > array[maxId]){
                maxId = i;
            }
        }
        return maxId;
    }

    @Override
    public long removeMax() {
        int maxId = getMaxId();
        long result = array[maxId];

        delete(result);

        return result;
    }

    public int noDups(){
        int dupCount = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if(j == i)
                    continue;
                if(array[i] == array[j]){
                    delete(array[j]);
                    dupCount++;
                    j--;
                }
            }
        }
        return dupCount;
    }
}