package array.app;

import array.interfaces.Array;
import array.interfaces.ArrayProcessor;
import array.process.ArrayProcessorImpl;
import array.process.ArrayProcessorProxy;
import array.sort.InsertArray;

import java.util.Scanner;

public class ArrayApp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int maxArraySize = sc.nextInt();
        int initialArraySize = sc.nextInt();
        Array array = new InsertArray(maxArraySize);

        ArrayProcessor arrayProcessor = new ArrayProcessorProxy(
                new ArrayProcessorImpl(
                        array, initialArraySize, sc
                )
        );

        /* тест вставка/поиск/удаление
        arrayProcessor.printArray();

        arrayProcessor.insert();

        arrayProcessor.find();

        arrayProcessor.delete();
        */

        /* тест removeMax()
        arrayProcessor.printArray();
        array.removeMax();
        arrayProcessor.printArray();
        */

        /* Сортировка
        Array sortedArray = new HighArray(maxArraySize);
        while(array.size() > 0){
            int id = array.size() - 1;
            long value = array.removeMax();
            sortedArray.insert(value, id);
        }

        sortedArray.printArray();
        */

        /* Тест merge()
        array.printArray();

        OrderArray orderArray = new OrderArray(15);


        System.out.println("Input:");
        for (int i = 0; i < 8; i++) {
            orderArray.insert(sc.nextLong());
        }
        orderArray.printArray();
        System.out.println("Output:");
        orderArray.merge(array).printArray();


        System.out.println();
        */

        /* Тест удаления дубликатов
        arrayProcessor.printArray();

        array.noDups();

        arrayProcessor.printArray();
        */

        arrayProcessor.sort();
        ((InsertArray) array).noDups();
        array.printArray();
    }
}