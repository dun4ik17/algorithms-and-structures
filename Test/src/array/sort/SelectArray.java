package array.sort;

import array.interfaces.AbstractArray;
import array.interfaces.Sort;

public class SelectArray extends AbstractArray implements Sort {

    public SelectArray(int maxSize){
        super(maxSize);
    }

    @Override
    public void sort() {
        for (int i = 0; i < size; i++) {
            int minId = i;
            for (int j = i; j < size; j++) {
                if(array[minId] > array[j]){
                    minId = j;
                }
            }
            swap(minId, i);
        }
    }
}