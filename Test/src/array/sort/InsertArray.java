package array.sort;

import array.interfaces.AbstractArray;
import array.interfaces.Sort;

public class InsertArray extends AbstractArray implements Sort {
    public InsertArray(int maxSize) {
        super(maxSize);
    }

    @Override
    public void sort() {
        for (int i = 1; i < size; i++) {
            long temp = array[i];
            int j = i;
            while (j > 0 && array[j - 1] >= temp) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = temp;
        }
    }

    public int median() {
        return size / 2;
    }

    public void noDups() {
        if (size < 2)
            return;

        int initialArraySize = size;
        boolean[] isDeleteValueId = new boolean[initialArraySize];

        sort();

        for (int i = 1; i < initialArraySize; i++) {
            if (array[i] == array[i - 1]) {
                isDeleteValueId[i] = true;
                size--;
            }
        }

        int targetId = 1;
        int deleteValuesCount = initialArraySize - size;
        int selectId = 1;

        for (int i = 0; i < initialArraySize; i++) {

            if(selectId + 1 == initialArraySize)
                break;
            while (targetId + 1 < initialArraySize && !isDeleteValueId[targetId])
                targetId++;
            while(selectId + 1 < initialArraySize && selectId <= targetId){
                selectId++;
            }
            while (selectId + 1 < initialArraySize && isDeleteValueId[selectId])
                selectId++;


            array[targetId] = array[selectId];
            isDeleteValueId[selectId] = true;
            isDeleteValueId[targetId] = false;
        }
    }
}