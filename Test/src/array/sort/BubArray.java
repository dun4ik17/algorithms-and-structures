package array.sort;

import array.interfaces.AbstractArray;
import array.interfaces.Sort;

public class BubArray extends AbstractArray implements Sort {
    public BubArray(int maxSize) {
        super(maxSize);
    }

//    @Override
//    public void sort(){
//        for (int i = size - 1; i >= 0 ; i--) {
//            for (int j = 0; j < i; j++) {
//                if(array[j] > array[j + 1])
//                    swap(j, j + 1);
//            }
//        }
//    }

    @Override
    public void sort(){
        int leftBound;
        int rightBound;
        int diff = 0;

        while(diff < size){
            leftBound = diff;
            rightBound = size - 1 - diff;

            for (int i = leftBound; i <= rightBound; i++) {
                if(i < size - 1 && array[i] > array[i + 1])
                    swap(i, i + 1);

            }

            for (int i = rightBound; i >= leftBound ; i--) {
                if(i > 0 && array[i] < array[i - 1])
                    swap(i, i - 1);
            }


            diff++;
        }
    }
}