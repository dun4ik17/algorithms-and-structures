package array.order;

import array.interfaces.AbstractArray;

public class OrderArray extends AbstractArray {

    private long[] array;
    private int size;

    public OrderArray(int maxSize){
        super(maxSize);
    }

    @Override
    public int insert(long value) {
        int resultId = -1; // -1 - "пустое" значение
        int targetId = -404; // -404 - "пустое" значение только тут
        int lowerBound = 0;
        int higherBound = size - 1;
        int middle;

        if(size == 0){
            array[0] = value;
        }
        else {
            while (true) {
                middle = (higherBound + lowerBound) / 2;
                if (array[middle] == value) {
                    targetId = middle;
                    break;
                } else if (value >= array[size - 1]) {
                    targetId = size - 1;
                    break;
                } else if (value < array[0]) {
                    targetId = -1;
                    break;
                } else if (value == array[0]) {
                    targetId = 0;
                    break;
                } else if (value > array[middle]) {
                    lowerBound = middle + 1;
                    if (value == array[lowerBound]) {
                        targetId = lowerBound;
                        break;
                    } else if (value < array[lowerBound]) {
                        targetId = lowerBound - 1;
                        break;
                    }
                } else if (value < array[middle]) {
                    higherBound = middle - 1;
                    if (value == array[higherBound]) {
                        targetId = higherBound;
                        break;
                    } else if (value > array[higherBound]) {
                        targetId = higherBound;
                        break;
                    }
                }
            }


            for (int i = size; i > targetId; i--) {
                if (i == targetId + 1) {
                    array[i] = value;
                    resultId = i;
                    break;
                }
                array[i] = array[i - 1];
            }
        }
        size++;
        return resultId;
    }

    @Override
    public int insert(long value, int id){
        return insert(value);
    }

    @Override
    public int find(long value) {
        int lowerBound = 0;
        int higherBound = size - 1;
        int middle;

        while(true){
             middle = (higherBound + lowerBound) / 2;
             if(array[middle] == value){
                 return middle;
             }
             else if(lowerBound > higherBound){
                 return -1;
             }
             else {
                 if(value < array[middle])
                     higherBound = middle - 1;
                 else
                     lowerBound = middle + 1;
             }
        }
    }

    @Override
    public boolean find(long value, boolean returnBoolean) {
        int findingId = find(value);

        return findingId != -1;
    }

    @Override
    public int delete(long value) {
        int findingId = find(value);

        if(findingId == -1)
            return -1;
        else {
            for (int i = findingId; i < size; i++) {
                if(i + 1 != size)
                    array[i] = array[i+1];
                else
                    array[i] = 0;
            }
            size--;
            return findingId;
        }
    }

    @Override
    public void printArray() {
        for (int i = 0; i < size; i++) {
            System.out.print(array[i] + " ");
        }
    }

    public OrderArray merge(OrderArray anotherArray){
        OrderArray generalArray = this;
        OrderArray secondArray = anotherArray;
        OrderArray resultArray =
                new OrderArray(generalArray.array.length + secondArray.array.length);

        if(size < anotherArray.size) {
            generalArray = anotherArray;
            secondArray = this;
        }

        for (int i = 0; i < generalArray.size; i++) {
            resultArray.insert(generalArray.array[i]);
            if(i < secondArray.size)
                resultArray.insert(secondArray.array[i]);
        }

        return resultArray;
    }
}
