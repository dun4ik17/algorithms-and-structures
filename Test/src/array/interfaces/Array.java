package array.interfaces;

public interface Array {
    int size();
    int insert(long value);

    int insert(long value, int id);
    int find(long value);
    boolean find(long value, boolean returnBoolean);
    int delete(long value);

    void printArray();
}
