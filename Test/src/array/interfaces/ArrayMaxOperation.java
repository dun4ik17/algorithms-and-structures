package array.interfaces;

public interface ArrayMaxOperation {
    int getMaxId();
    long removeMax();
}
