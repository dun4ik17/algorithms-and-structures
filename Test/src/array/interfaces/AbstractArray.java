package array.interfaces;

public abstract class AbstractArray implements Array{

    protected long[] array;
    protected int size;

    public AbstractArray(int maxSize){
        array = new long[maxSize];
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int insert(long value) {
        array[size] = value;
        size++;
        return size - 1;
    }

    @Override
    public int insert(long value, int id){
        array[id] = value;
        size++;
        return id;
    }

    @Override
    public int find(long value) {
        int i;

        for (i = 0; i < size; i++) {
            if(array[i] == value)
                break;
        }
        if(i == size)
            return -1;
        else
            return i;
    }

    @Override
    public boolean find(long value, boolean returnBoolean){
        int result = find(value);

        return result != -1;
    }

    @Override
    public int delete(long value) {
        int id = find(value);

        if(id == -1)
            return -1;
        else{
            for (int i = id; i < size; i++) {
                if(i + 1 != size)
                    array[i] = array[i+1];
                else
                    array[i] = 0;
            }
            size--;
            return id;
        }
    }
    @Override
    public void printArray(){
        for (int i = 0; i < size; i++) {
            if(i % 10 == 0)
                System.out.println();
            System.out.print(array[i] + " ");
        }
    }

    protected void swap(int firstId, int secondId){
        long temp = array[firstId];
        array[firstId] = array[secondId];
        array[secondId] = temp;
    }
}
