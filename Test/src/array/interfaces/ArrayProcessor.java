package array.interfaces;

public interface ArrayProcessor {

    void insert();
    int find();
    int delete();

    void printArray();

    void printMax();

    void sort();
}
