package array.process;

import array.interfaces.Array;
import array.interfaces.ArrayMaxOperation;
import array.interfaces.ArrayProcessor;

public class ArrayProcessorProxy implements ArrayProcessor {

    private ArrayProcessor arrayProcessor;

    public ArrayProcessorProxy(ArrayProcessor arrayProcessor){
        this.arrayProcessor = arrayProcessor;
    }


    public void isNumberPresentAndWriteTime(int resultId, long time){
        if(resultId == -1)
            System.out.println("Number is not exist");
        else {
            System.out.println("Found the number - " + resultId);
            System.out.println("Time for this operation is - " + time);
        }
    }

    @Override
    public void insert() {
        System.out.println("Enter number to insert");
        long time = System.nanoTime();
        arrayProcessor.insert();
        time = System.nanoTime() - time;

        System.out.println("Time for this operation is - " + time);

        printArray();
    }

    @Override
    public int find() {

        System.out.println("Enter number to find");
        long time = System.nanoTime();
        int resultId = arrayProcessor.find();
        time = System.nanoTime() - time;

        isNumberPresentAndWriteTime(resultId, time);

        return 0;
    }

    @Override
    public int delete() {
        System.out.println("Enter number to delete");
        long time = System.nanoTime();
        int resultId = arrayProcessor.delete();
        time = System.nanoTime() - time;

        isNumberPresentAndWriteTime(resultId, time);

        printArray();
        return 0;
    }

    @Override
    public void printArray(){
        arrayProcessor.printArray();
    }

    @Override
    public void printMax() {

        if(((ArrayProcessorImpl)arrayProcessor).getArray() instanceof ArrayMaxOperation) {
            System.out.println("Find maximum....");
            long time = System.nanoTime();
            arrayProcessor.printMax();
            time = System.nanoTime() - time;
            System.out.println("Time for this operation is - " + time);
        }
        else
            System.out.println("Array doesn't implement ArrayMaxOperation");
    }

    @Override
    public void sort() {
        System.out.println("Sorting array...");
        long time = System.nanoTime() / 1000; // mcs
        arrayProcessor.sort();
        time = System.nanoTime() / 1000 - time; // mcs
        printArray();
        System.out.println("Time for this operation is - " + time);
    }
}