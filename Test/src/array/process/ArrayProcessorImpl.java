package array.process;

import array.interfaces.Array;
import array.interfaces.ArrayMaxOperation;
import array.interfaces.ArrayProcessor;
import array.interfaces.Sort;
import java.util.Scanner;

public class ArrayProcessorImpl implements ArrayProcessor {
    protected Array array;
    private Scanner sc;

    public ArrayProcessorImpl(Array array, int initialArraySize, Scanner sc){
        this.array = array;
        this.sc = sc;

        if(initialArraySize > 0) {
            System.out.println("Enter first numbers of array");
            for (int i = 0; i < initialArraySize; i++) {
                array.insert(sc.nextLong());
            }
        }
    }

    @Override
    public void insert(){
        array.insert(sc.nextLong());
    }

    @Override
    public int find(){
        return array.find(sc.nextLong());
    }

    @Override
    public int delete(){
        return array.delete(sc.nextLong());
    }

    @Override
    public void printArray(){
        System.out.println("Output array: ");
        array.printArray();
        System.out.println();
    }

    @Override
    public void printMax(){
        int result = ((ArrayMaxOperation) array).getMaxId();

        if (result == -1)
            System.out.println("Array is empty");
        else
            System.out.println("Max id - " + result);
    }

    @Override
    public void sort() {
        if(array instanceof Sort){
            ((Sort) array).sort();
        }
        else {
            System.out.println("Array doesn't implement Sort");
        }
    }
    Array getArray(){
        return array;
    }
}