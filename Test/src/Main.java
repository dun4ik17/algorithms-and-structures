import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static String getString() throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String resultString = bufferedReader.readLine();
        return resultString;
    }

    public static double getDouble() throws IOException {
        String s = getString();
        Double d = Double.valueOf(s);
        return d.doubleValue();
    }
    public static void main(String[] args) throws IOException {
//        System.out.println(getString());
    }
}